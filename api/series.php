<?php
class Series {
    function __construct() {
        try {
            $this->db = new PDO('sqlite:../db');
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            die('DB error: '.$e->getMessage());
        }
    }
}
?>
