<?php
header('Content-Type: text/plain; charset=utf-8'); // Ž
require_once 'series.php';

class Add extends Series {
    private function id() {
        $q = 'SELECT `id` FROM `data` ORDER BY `id` DESC';
        $st = $this->db->prepare($q);
        $st->execute();
        $id = $st->fetchColumn();
        return ++$id;
    }

    function go($data) {
        $data['id'] = (int)$this->id();

        $q = 'INSERT INTO `data` (`id`, `nme`, `data`) VALUES (:id, :nme, :data)';
        $st = $this->db->prepare($q);
        $st->bindValue(':id', $data['id'], PDO::PARAM_INT);
        $st->bindValue(':nme', $data['nme']);
        $data = json_encode($data);
        $st->bindValue(':data', $data);
        $st->execute();

        echo $data;
    }
}

call_user_func_array(array(new Add(), $_GET['f']), $_GET['p']);
?>
