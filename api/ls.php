<?php
header('Content-Type: text/plain; charset=utf-8');
require_once 'series.php';

class Ls extends Series {
    function go() {
        $r = '[';

        $q = 'SELECT `data` FROM `data` ORDER BY `nme` ASC';
        $st = $this->db->prepare($q);
        $st->execute();
        while ($data = $st->fetchColumn()) {
            $r .= $data.',';
        }

        $r = substr_replace($r, ']', -1);

        echo $r;
    }
}

call_user_func_array(array(new Ls(), $_GET['f']), $_GET['p']);
?>
