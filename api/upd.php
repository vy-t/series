<?php
header('Content-Type: text/plain; charset=utf-8'); // Ž
require_once 'series.php';

class Upd extends Series {
    function go($data) {
        $q = 'UPDATE `data` SET `nme` = :nme, `data` = :data
            WHERE `id` = :id';
        $st = $this->db->prepare($q);
        $st->bindValue(':id', $data['id'], PDO::PARAM_INT);
        $st->bindValue(':nme', $data['nme']);
        $data = json_encode($data);
        $st->bindValue(':data', $data);
        $st->execute();
        echo $data;
    }
}

call_user_func_array(array(new Upd(), $_GET['f']), $_GET['p']);
?>
