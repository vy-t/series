var series = {};

series.zp = function(n) {
    return n.length >= 2 ? n : '0'+n;

};

series.ln = function(data) {
    var ln = $('<div>').addClass('ln').attr('data-id', data.id);

    var nme = $('<input>').attr('type', 'text').val(data.nme);
    var s = $('<input>').attr('type', 'text').val(this.zp(data.sn[0]));
    var e = $('<input>').attr('type', 'text').val(this.zp(data.sn[1]));

    ln.html([nme, s, e]);

    return ln;
};

series.get = function() {
    $('#main').html('');
    api('ls', 'go', [''], function(r) {
        series.data = r;

        r.forEach(function(v, k) {
            $('#main').append(series.ln(v));
        });
    });
};

series.lstn = function() {
    $('#add').click(function(e) {
        e.preventDefault();

        var li = $(this).parent('li');
        var ico = $('.glyphicon', $(this));

        if ($('#addrow').length) {
            $('#addrow').remove();
            return;
        }

        var addrow = series.ln({nme: '', sn: [0, 0], id: 0}).attr('id', 'addrow');

        $('#main').prepend(addrow);
    });

    $('#main').on('click', '.ln input', function(e) {
        $(this).select();
    });

    $('#main').on('change', '.ln input', function(e) {
        series.upd($(this));
    });
};

series.getdata = function(row) {
    var data = {};
    data.nme = $('> input:eq(0)', row).val();

    data.sn = [
        parseInt($('> input:eq(1)', row).val()),
        parseInt($('> input:eq(2)', row).val()),
    ];

    data.id = row.attr('data-id');

    return data;
};

series.upd = function(that) {
    var ln = that.parents('.ln');
    var data = this.getdata(ln);
    var method = data.id != 0 ? 'upd' : 'add';
    var n = that.index();
    var inp = $('input', ln);

    if (method === 'add') inp.prop('disabled', true);

    api(method, 'go', [data], function(r) {
        console.log(r);

        if (n === 1 || n === 2) that.val(series.zp(that.val()));

        if (method == 'add') {
            ln.attr('data-id', r.id);
            ln.removeAttr('id');
            inp.prop('disabled', false);
        }

        ln.addClass('bg-success');
        inp.addClass('bg-success');
        that.blur();
        setTimeout(function(){
            ln.removeClass('bg-success');
            inp.removeClass('bg-success');
        }, 500);
    });
};

series.get();
series.lstn();
